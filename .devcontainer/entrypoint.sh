#!/bin/bash

echo "Waiting for DB"
./.devcontainer/wait-for-it.sh -t 30 db:5432 -- echo "DB READY"

echo "Starting Golang Application"
exec go run main.go