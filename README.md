# Banking Application - a touch of Golang

## Credentials

Database:
- Instance: PostgreSQL
- Port: 5001
- Username: bank
- Password: bank
- Database: bank

Application:
- Language: Golang
- Port: 8001

## API

Endpoints:

`/login`
- You can test this by using PostMan to send a POST request with a JSON body containing:
    ```
    {
        "Username": "Martin",
        "Password": "Martin"
    }
    ```

## Installation
This application requires [docker](https://www.docker.com/) and [docker-compose](https://docs.docker.com/compose/).

It is recommended that you develop from within the container using [Visual Studio Code](https://code.visualstudio.com/) and the extensions `Remote Containers`

1. Navigate to the `docker-compose.yml` directory and `up` our containers:
    ```
        cd ./devcontainer
        docker-compose up -d
    ```
2. Install the recommended extensions prompted.
    - Remote - Containers (ms-vscode-remote.remote-containers)
3. Restart Visual Studio Code
4. When prompted to reopen inside the container click "Reopen in Container"

This will now install all the required extensions.

*Further extensions may be required for Go, Visual Studio Code will prompt you when required, feel free to install these manually or using the IDE.*

## Code Tours

Code tours are available for various implementation details. You can access these from the explorer under "CODETOUR". 

Upon the first time opening you will be asked if you want to begin the tour, this will run you through some of the basics of Golang.

## Reference material

- Golang API tutorial:
    - [Lesson 1](https://www.blog.duomly.com/golang-course-with-building-a-fintech-banking-app-lesson-1-start-the-project/)
    - [Lesson 2](https://www.blog.duomly.com/golang-course-with-building-a-fintech-banking-app-lesson-2-login-and-rest-api/)