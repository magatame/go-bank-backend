package main

import "bitbucket.org/magatame/go-bank-backend/migrations"
import "bitbucket.org/magatame/go-bank-backend/api"

func main() {
    migrations.Migrate()
    api.StartApi()
}
