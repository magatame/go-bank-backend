package migrations

import (
	"bitbucket.org/magatame/go-bank-backend/helpers"
	"bitbucket.org/magatame/go-bank-backend/interfaces"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

func Migrate() {
	User := &interfaces.User{}
	Account := &interfaces.Account{}
	db := helpers.ConnectDB()
	db.AutoMigrate(User, Account)
	defer db.Close()

	createAccounts()
}

func createAccounts() {
	db := helpers.ConnectDB()

	users := &[2]interfaces.User{
		{Username: "Martin", Email: "martin@martin.com"},
		{Username: "Michael", Email: "michael@michael.com"},
	}

	for i := 0; i < len(users); i++ {
		generatedPassword := helpers.HashAndSalt([]byte(users[i].Username))
		user := &interfaces.User{Username: users[i].Username, Email: users[i].Email, Password: generatedPassword}
		db.FirstOrCreate(user)

		account := &interfaces.Account{Type: "Daily Account", Name: string(users[i].Username + "'s" + " account"), Balance: uint(10000 * int(i+1)), UserId: user.ID}
		db.FirstOrCreate(account)
	}
	defer db.Close()
}
